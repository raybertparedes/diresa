# Sistemas de Información DIRESA

De acuerdo al diagnóstico y levantamiento de información realizado, considerando que ya hay varios sistemas que se han implementado para mejorar el desempeño de las distintas áreas, se ha podido determinar la necesidad de los siguientes sistemas de información: 

1. Portal web institucional
2. Intranet institucional
3. Sistema para el control de metas-proyectos
4. Sistema de trámite documentario
5. Sistema de aula virtual
6. Sistema de digitalización de archivo
7. Sistema de escalafón
8. Sistema para el control de vehículos, repuestos e insumos
9. Sistema para inspecciones sanitarias
10. Sistema de caja
11. Sistema integrador de información administrativa (SIAF,SIGA)
12. Sistema integrador de información de recursos humanos
13. Sistema gerencia? para alta dirección

El Proyecto supone contar con una plataforma tecnológica de soporte para los procesos de la Organización de forma adecuada y suficiente, que permitirán mejorar y optimizar las funciones propias de las áreas administrativas y de servicios de manera integral para Dirección Regional de Salud. 

El sistema de información deberá ser es escalable, integral de interfaces amigables y basado en un entorno tecnológico moderno con lo que se lograra alcanzar los objetivos estratégicos de la organización. 

El Componente de Adecuados Sistemas Tecnológicos de Información y Comunicación ha sido elaborado en concordancia con el programa de necesidades de la Dirección Regional de Salud. 

Especificaciones técnicas de la plataforma de desarrollo.
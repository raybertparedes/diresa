
insert into diresa_admin.aplicacion (nombre_aplicacion, alias_aplicacion)
    values ('Tramite documentario', 'TRADOC');

insert into diresa_admin.aplicacion (nombre_aplicacion, alias_aplicacion)
    values ('Portal Web institucional', 'PORWEB');

insert into diresa_admin.aplicacion (nombre_aplicacion, alias_aplicacion)
    values ('Intranet institucional', 'INTINS');

insert into diresa_admin.rol (id_aplicacion, nombre_rol, alias_rol)
    values (1, 'Tramitador', 'TR');

insert into diresa_admin.rol (id_aplicacion, nombre_rol, alias_rol)
    values (1, 'Visualizador', 'VI');

insert into diresa_admin.modulo (nombre_modulo, alias_modulo, id_aplicacion)
    values ('Gestion de procedimientos', 'GESPRO', 1);

insert into diresa_admin.modulo (nombre_modulo, alias_modulo,id_aplicacion)
    values ('Gestion de documentos externos', 'GESEXT', 1);

insert into diresa_admin.modulo (nombre_modulo, alias_modulo, id_aplicacion)
    values ('Gestion de documentos internos', 'GESINT', 1);

insert into diresa_admin.modulo (nombre_modulo, alias_modulo, id_aplicacion)
    values ('Gestion de resoluciones', 'GESTRES', 1);

insert into diresa_admin.accion (nombre_accion, alias_accion)
    values ('Iniciar tamite', 'INICTRAM');

insert into diresa_admin.accion (nombre_accion, alias_accion)
    values ('Seguimiento tamite', 'SEGUTRAM');

insert into diresa_admin.rol_modulo (id_modulo, id_rol)
    values (2, 1), (3, 1), (3, 2), (4, 2);

insert into diresa_admin.modulo_accion (id_modulo, id_accion)
    values (2, 2), (3, 1), (4, 2), (4, 1);

insert into diresa_admin.rol_accion (id_modulo, id_accion, id_rol)
    values (1, 1, 1), (1, 2, 2), (2, 2, 2), (2, 1, 1);

insert into diresa_admin.persona (nombre_persona,nombres,apellidos,correo_electronico,fecha_nacimiento,dni,domicilio,telefono)
    values ('Geraldine Marquez','Geraldine','Marquez','eget.massa@blanditatnisi.ca','1988-12-05','37324829','Cras convallis convallis dolor. Quisque tincidunt pede ac','051-557-220');

insert into diresa_admin.persona (nombre_persona,nombres,apellidos,correo_electronico,fecha_nacimiento,dni,domicilio,telefono)
    values ('Elizabeth Valdez','Elizabeth','Valdez','sem@Nullamsuscipitest.com','1999-01-12','22064576','Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque','974-301-963');

insert into diresa_admin.persona (nombre_persona,nombres,apellidos,correo_electronico,fecha_nacimiento,dni,domicilio,telefono)
    values ('Velma Ortega','Velma','Ortega','adipiscing.elit.Etiam@variusultrices.co.uk','1956-06-01','18274732','Morbi vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis','818-784-539');

insert into diresa_admin.persona (nombre_persona,nombres,apellidos,correo_electronico,fecha_nacimiento,dni,domicilio,telefono)
    values ('Miranda Soto','Miranda','Soto','eget.massa@Integeridmagna.com','1952-05-07','28210708','sodales at, velit. Pellentesque ultricies dignissim','656-532-807');

insert into diresa_admin.usuario (id_persona, nombre_usuario, clave)
    values (1, 'gmarquez', '==dsadasn5436FDsdfyfdSSGd');

insert into diresa_admin.usuario (id_persona, nombre_usuario, clave)
    values (2, 'evaldez', '==dsadasn543FDsd54654fsa');

insert into diresa_admin.usuario (id_persona, nombre_usuario, clave)
    values (3, 'vortega', '==dsadasn324&654fddgfsha');

insert into diresa_admin.usuario (id_persona, nombre_usuario, clave)
    values (4, 'msoto', '==dsadasn3453kknnnjnjda');

insert into diresa_admin.rol_usuario (id_usuario, id_rol)
    values (1, 1), (1, 2), (2, 1), (2, 2), (3, 2);

insert into diresa_admin.log_auditoria (id_aplicacion, id_usuario, id_rol, id_modulo, id_accion, evento, observacion)
    values (1, 1, 1, 1, 1, 'Creacion', 'Insersion de nuevo tramite DRM-0446');

insert into diresa_admin.log_auditoria (id_aplicacion, id_usuario, id_rol, id_modulo, id_accion, evento, observacion)
    values (1, 1, 1, 1, 1, 'Eliminacion', 'Eliminacion de tramite DRM-0545');

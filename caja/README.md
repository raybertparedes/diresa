# Requerimiento - Sistema de caja
## Justificación
Actualmente no se cuenta con un sistema que apoye las operaciones en caja, respecto al ingreso de dinero a las arcas de la institución. El control que actualmente se realiza, es mediante el registro de información en hojas de cálculo Excel, lo cual genera bastante vulnerabilidad en el resguardo de la información, así como evita el acceso a los datos por parte de otros trabajadores que también necesitan de dicha información. 
## Alcance
Oficina de Caja. 
## Objetivos del Sistema
- Tener un sistema que permita registrar y llevar un control adecuado respecto al dinero que ingresa en las arcas de la DIRESA.
- De acuerdo a ciertos niveles de seguridad, permitir el acceso compartido a la información de ingresos de dinero. 
## Descripción general
Este sistema deberá complementar las acciones que se realizan en el sistema integrado de administración financiera SIAF, es decir, deberá registrar todos los ingresos de dinero que tiene la institución y llevar el control adecuado del mismo, según los requerimientos del área de recaudación. El sistema deberá permitir la impresión de comprobantes de ingresos, así como diferentes reportes que ayuden en el control financiero. 
## Plan de Trabajo
```mermaid
gantt
    title Sistema de Caja
    dateFormat  DD-MM-YYYY
    section 1. Recoleción de información
    AP1,P1 : 01-11-2019, 2d
    section 2. Analisis de requerimeintos
    AP1,P1 : 02-11-2019, 2d
    section 3. Diseño y documentación de base de datos
    AP1,P1 : 03-11-2019, 2d
    section 4. 
    Diseño de interfaz : 28-10-2019, 0d
    section 5. Desarrollo del sistema
    Registro :  05-11-2019, 4d
    Búsqueda : 09-11-2019, 4d
    section 6. Pruebas del sistema y corrección de errores,
    AP1 : 13-11-2019, 3d
    section 7. Elaboración de manuales
    P1 : 09-11-2019, 8d
    section 8. Capacitación de usuarios
    AP1 : 13-11-2019, 3d
   section 9. Puesta enmarcha del sistema
    AP1 : 17-11-2019, 1d
```
## Interactividad con otros sistemas
- Intranet institucional.
- Sistema integrador de información administrativa. 
## Servicios a brindar por la web
Ninguno. 
## Especificaciones técnicas mínimas
A continuación se detalla el listado mínimo de información qué Se deberá tratar: Registro de centros de costo. Registro de clasificadores de ingresos. Registro de ingresos de dinero a la institución. Impresión de comprobantes de ingreso. Generación de reportes de control. 
# Arquitectura

##	Gestión de Procedimientos
En esta sección se pide poder administrar los componentes que interfieren en el Sistema de Caja, es decir, poder registrar, editar e inhabilitar un órgano, un clasificador, un concepto de clasificador y un solicitante.

- Buscar órgano (Código, Nombre)
- Registrar órgano (Código, Nombre)
- Editar órgano: Permitir editar el nombre
- Buscar clasificador de ingreso (Código órgano, código clasificador, descripción)
- Registrar clasificador de ingreso (Código órgano, código clasificador, descripción)
- Editar clasificadores de ingreso: (Permitir editar solo la descripción)
- Inhabilitar clasificadores de ingreso
- Registrar CONCEPTO perteneciente a un clasificador según TUPA o TUSNE, consignar los siguientes datos: Clasificador, Nombre del concepto, Monto,
- Editar concepto: Permitir editar todos los campos.
- Inhabilitar concepto
- Buscar solicitante (Documento de identidad.  Nombres, Apellidos)
- Registrar solicitante (Documento de identidad. Pueden ser: DNI, RUC, Carnet de Extranjería, nombres, apellidos)
- Editar solicitante: Permitir editar todos los campos
- Inhabilitar solicitante
## Gestión de Ingresos
En este apartado se efectuarán los ingresos de pagos. Además, antes de generar u ingreso se deberá poder tener un acceso directo al registro de un nuevo solicitante, solo en caso de no encontrarse registrado.
- Acceso directo al registro de un nuevo solicitante
- Registrar un nuevo ingreso, se considerarán los siguientes datos: N° correlativo de ingreso (asignado por el sistema), Fecha (predeterminado hoy), Tipo de ingreso (ingreso externo o devolución), Fuente de ingreso, Concepto (en caso de ser del tipo devolución), Solicitante (seleccionar uno de la Base de datos o agregar uno nuevo), Se podrán considerar varios conceptos en un mismo ingreso, estos considerarán los siguientes datos: Cantidad, Concepto,  Monto unitario, Subtotal, Monto total (autosuma después de seleccionar los conceptos de pago)

Datos tomados del siguiente gráfico:

![Recibo de Caja](recibo_caja.jpg  "Recibo de Caja")

- Impresión del ingreso en formato boleta de CAJA. 

![Recibo INgresos](recibo_ingresos.jpg  "Recibo Ingresos")

## REPORTES
Se pide generar reportes por órgano, clasificador, concepto y solicitante en rangos de fechas seleccionados por el usuario.
- Reporte general, tendrá los siguientes filtros: Rango de fechas, Órgano, Tipo de ingreso, Clasificador, Concepto, Fuente de ingreso, Solicitante, 

Y mostrará lo datos según el siguiente gráfico:

 
Ilustración 5 Recibo de Ingresos Presupuestado.

- Reporte diario, de acuerdo a la Ilustración 4.
- Reporte de gastos agrupados por ÓRGANO. Se tendrán los siguientes filtros:
- Rango de fechas

Tabla 2 Estructura del reporte agrupado por órgano.
 
![Recibo Agrupado por Organo](reporte_agrupado.jpg  "Recibo Agrupado por Organo")
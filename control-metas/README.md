# Sistema para control de metas
## Justificación
Los altos directivos de la DIRESA o responsables de cada área no saben de forma real el estado de avance de las labores que realiza cada trabajador bajo su cargo, de tal forma que permita medir la eficiencia y desempeño de cada trabajador, así como en conjunto por oficina. 
## Alcance
Todas las oficinas de la DIRESA. 
## Objetivos del Sistema
- Poseer un sistema que permita medir el estado de avance de las tareas encargadas a cada trabajador. 
- Identificar los principales obstáculos que no permiten mejorar la eficiencia de la gestión administrativa. 

## Descripción general 
El presente sistema tiene por finalidad mostrar el estado de gestión o cumplimiento de metas en la institución a través del registro y control de las tareas del personal en las diferentes oficinas de la institución. A través de este sistema cada trabajador al inicio de cada mes, deberá registrar su plan de trabajo mensual y conforme vaya realizando cada tarea, ir descargándolas del sistema, así mismo, aquellas tareas que por diferentes razones no se pudieron concretar, se deberá poder registrar la razón por la cual no se cumplió con la meta expuesta. De esta manera el sistema debe permitir consolidar esta información por persona, oficina, dirección o grupo de oficinas, y al final si juntamos las estadísticas de todos los trabajadores, se deberá poder obtener el estado de metas como DIRESA en general. Así de esta manera se puede conocer el estado situacional de la institución respecto al trabajo que realiza. 

## Plan de Trabajo
```mermaid
gantt
    title Sistema de Caja
    dateFormat  DD-MM-YYYY
    section 1. Recoleción de información
    AP1,P1 : 01-11-2019, 2d
    section 2. Analisis de requerimeintos
    AP1,P1 : 02-11-2019, 2d
    section 3. Diseño y documentación de base de datos
    AP1,P1 : 03-11-2019, 2d
    section 4. 
    Diseño de interfaz : 28-10-2019, 0d
    section 5. Desarrollo del sistema
    Registro :  05-11-2019, 4d
    Búsqueda : 09-11-2019, 4d
    section 6. Pruebas del sistema y corrección de errores,
    AP1 : 13-11-2019, 3d
    section 7. Elaboración de manuales
    P1 : 09-11-2019, 8d
    section 8. Capacitación de usuarios
    AP1 : 13-11-2019, 3d
   section 9. Puesta enmarcha del sistema
    AP1 : 17-11-2019, 1d
```
## Interactividad con otros Sistemas
- lntranet institucional.
- Sistema de información gerencia.

## Servicios a brindar por la web
Ninguno.

## Especificaciones técnicas mínimas 
Se deberá tomar en cuenta la implementación de los siguientes puntos:
- Registro de planes de trabajo por parte del personal.
- Registro de tareas cumplidas y justificación de las no cumplidas.
- Control de plazos vencidos y por vencer.
- Consolidación de datos y estadísticas por persona y oficina. 